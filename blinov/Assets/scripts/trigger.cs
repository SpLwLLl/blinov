using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class trigger : MonoBehaviour


{
    public GameObject Cube;
    public Transform Spawn;

    private void OnTriggerExit(Collider other)
        
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Trigger on");
            Instantiate(Cube, Spawn.position, Quaternion.identity);

            SceneManager.LoadScene(0);


        }
        
    }



}
