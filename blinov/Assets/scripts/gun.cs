using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class gun : MonoBehaviour
{
    public SteamVR_Action_Boolean fireAction;
    public GameObject bullet;
    public Transform barrePivot;
   // public GameObject muzzleFlash;
    public float shootingSpeed = 3;

       private Interactable interactible;
      void Start()
      {
        // muzzleFlash.SetActive(false);
        interactible = GetComponent<Interactable>();

      }
    
    void Update()
    {
       if(interactible.attachedToHand != null)
       {
            SteamVR_Input_Sources source = interactible.attachedToHand.handType;
            if (fireAction[source].stateDown)
            {
                Fire();
            }    
       }

    }
    public void Fire()
    {
        Rigidbody BulletRigidbody = Instantiate(bullet, barrePivot.position, barrePivot.rotation). GetComponent<Rigidbody>();
        BulletRigidbody.velocity = barrePivot.up * shootingSpeed;
       // muzzleFlash.SetActive(false);



    }




}
