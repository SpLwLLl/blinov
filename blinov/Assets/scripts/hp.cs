using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class hp : MonoBehaviour
{
     public float hitpoint = 100;
    public TMP_Text hitpointText;


    void Start()
    {
        hitpointText.text = ((int)hitpoint).ToString();

    }  

    void UpdateHitpointText()
    {
        hitpointText.text = ((int)hitpoint).ToString();
    }
    void Update()
    {
        UpdateHitpointText();

        if (hitpoint >= 100)
        {
            hitpoint = 100;
        }
        if (hitpoint < 0)
        {
            SceneManager.LoadScene("Lose");
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "DAMAGE")
        {

            hitpoint -= 7;

        }
        if (collision.gameObject.tag == "hp")
        {

            hitpoint += 50;

        }
    }   
    



}
