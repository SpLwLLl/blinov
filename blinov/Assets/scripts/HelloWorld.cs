using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    
    public float Timer;
    public GameObject box;
    public Transform SpawnPoint;

    // Start is called before the first frame update
    void Start()
    {

        

    }
    // Update is called once per frame
    void Update()
    {

        Timer += Time.deltaTime;

        if (Timer > 2)
        {
            Instantiate(box, SpawnPoint.position, Quaternion.identity);
            Timer = 0;
        }
    }

}