using System.Collections;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IQ : MonoBehaviour
{
    public Transform Player;
    float distance;
    NavMeshAgent myAgent;
    Animator myAnim;
    public float hp = 100;

    private void Awake()
    {
        if (Player == null)
        {
            Player = GameObject.FindWithTag("Player").transform;
        }
    }
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }

    
   private void Update()
    {
        if (hp <= 0)
        {
            myAnim.Play("kill");
            enabled = false;
        }
        distance = Vector3.Distance(transform.position, Player.position);

        if (distance <= 70)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("ATTAKS", false);
            myAnim.SetBool("RUN", true);
        }
        if (distance <= 3)
        {
            myAgent.enabled = false;
            myAnim.SetBool("ATTAKS", true);
            myAnim.SetBool("RUN", false);
        }

    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "npcdamage")
        {
            hp -= 100;
        }
    }
}




